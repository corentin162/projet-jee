@echo off
call mvn clean package
call docker build -t com.efficom/projetJee .
call docker rm -f projetJee
call docker run -d -p 9080:9080 -p 9443:9443 --name projetJee com.efficom/projetJee