#!/bin/sh
if [ $(docker ps -a -f name=projetJee | grep -w projetJee | wc -l) -eq 1 ]; then
  docker rm -f projetJee
fi
mvn clean package && docker build -t com.efficom/projetJee .
docker run -d -p 9080:9080 -p 9443:9443 --name projetJee com.efficom/projetJee
