package com.efficom.managed;

import com.efficom.Dao.UserDao;
import com.efficom.users.User;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;

@ManagedBean
@RequestScoped
public class ManagedUserBean implements Serializable {

    private User user = new User();
    @EJB
    private UserDao userDao;

    public String create() throws Exception {
        userDao.createUser(user);
        FacesMessage message = new FacesMessage("Succès de l'inscription !");
        FacesContext.getCurrentInstance().addMessage(null, message);
        return "loginPage.xhtml";
    }

    public String connect() throws Exception {
        User userConnect = userDao.searchUser(user);
        if (userConnect != null) {
            FacesMessage message = new FacesMessage("Connexion réussi !");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return "themeSwitcher.xhtml";
        }
        else{
            FacesMessage message = new FacesMessage("Email ou mot de passe incorrect !");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        return "loginPage.xhtml";

    }


    public User getUser() {
        return user;
    }


}

