package com.efficom.users;

import com.efficom.exercise.Exercise;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class User {

    @Id
    @GeneratedValue
    private Long idUser;

    private String name;
    private String firstName;
    @Pattern( regexp = "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)", message = "Merci de saisir une adresse mail valide" )
    private String email;
    @Size( min = 8, message = "Le mot de passe doit contenir au moins 8 caractères" )
    private String password;

  /*  @OneToMany(targetEntity = Exercise.class, mappedBy = "userExercise")
    private List<Exercise> exerciseList = new ArrayList<>();*/

}
