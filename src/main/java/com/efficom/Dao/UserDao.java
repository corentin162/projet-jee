package com.efficom.Dao;


import com.efficom.users.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class UserDao {

    private static final String JPQL_SELECT_PAR_EMAIL_ET_MDP = "SELECT u FROM User u WHERE u.email=:email and u.password= :password";
    private static final String JPQL_SELECT_ALL_USERS = "SELECT u FROM User u ";
    private static final String JPQL_SELECT_PAR_EMAIL = "SELECT u FROM User u WHERE u.email=:email";
    private static final String PARAM_EMAIL = "email";
    private static final String PARAM_PASSWORD = "password";

    @PersistenceContext(unitName = "prod")
    private EntityManager em;

    public List<User> getAllUsers() throws Exception {
        List<User> users;
        Query requete = em.createQuery(JPQL_SELECT_ALL_USERS);
        try {
            users = requete.getResultList();
        } catch (NoResultException e) {
            return null;
        } catch (Exception e) {
            throw new Exception(e);
        }
        return users;
    }

    public void createUser(User user) throws Exception {
        try {
            em.persist(user);
        } catch (Exception e) {
            throw new Exception(e);

        }
    }

    public User getUserWithMail(String email) throws Exception {
        User user = null;
        Query requete = em.createQuery(JPQL_SELECT_PAR_EMAIL);
        requete.setParameter(PARAM_EMAIL, email);
        try {
            user = (User) requete.getSingleResult();
        } catch (NoResultException e) {
            return null;
        } catch (Exception e) {
            throw new Exception(e);
        }
        return user;

    }

    public User searchUser(User user) throws Exception {
        Query requete = em.createQuery(JPQL_SELECT_PAR_EMAIL_ET_MDP);
        requete.setParameter(PARAM_EMAIL, user.getEmail());
        requete.setParameter(PARAM_PASSWORD, user.getPassword());
        try {
            user = (User) requete.getSingleResult();
        } catch (NoResultException e) {
            return null;
        } catch (Exception e) {
            throw new Exception(e);
        }
        return user;
    }
}
