package com.efficom.api;

import com.efficom.Dao.UserDao;
import com.efficom.users.User;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("api")
public class apiResource {

    @Inject
    @ConfigProperty(name = "message")
    private String message;

    @Inject
    private UserDao userDao;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/allUsers")
    public Response getUsers() throws Exception {
        List<User>  allusers = userDao.getAllUsers();
        return Response.ok(allusers).build();
    }
}
