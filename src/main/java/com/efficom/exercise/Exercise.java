package com.efficom.exercise;

import com.efficom.users.User;

import javax.persistence.*;

@Entity
public class Exercise {

    @Id
    @GeneratedValue
    private Long idExercise;

    private String nameExercise;

    @ManyToOne
    @JoinColumn(name = "idUser", nullable = true)
    private User userExercise;

    public Long getIdExercise() {
        return idExercise;
    }

    public void setIdExercise(Long idExercise) {
        this.idExercise = idExercise;
    }

    public String getNameExercise() {
        return nameExercise;
    }

    public void setNameExercise(String nameExercise) {
        this.nameExercise = nameExercise;
    }

    public User getUserExercise() {
        return userExercise;
    }

    public void setUserExercise(User userExercise) {
        this.userExercise = userExercise;
    }


}
